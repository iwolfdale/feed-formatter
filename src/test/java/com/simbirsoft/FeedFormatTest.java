package com.simbirsoft;

import com.simbirsoft.format.model.Feed;
import com.simbirsoft.format.exception.FeedFormatException;
import com.simbirsoft.format.exception.FeedFormatExceptionType;
import com.simbirsoft.format.Formatter;
import com.simbirsoft.format.wrapper.impl.AbstractTagWrapper;
import org.junit.Assert;
import org.junit.Test;

public class FeedFormatTest {

    private FeedFormatExceptionType getExceptionByProcessFeed(Formatter formatter, Feed feed) {
        FeedFormatExceptionType type = null;
        try {
            formatter.processFeed(feed);
        } catch (FeedFormatException e) {
            type = e.getType();
        }
        return type;
    }

    @Test
    public void testIntersectData() {
        String data = FeedService.loadDataFromResourceFile("test_intersect.json");
        Feed feed = FeedService.readSingleFeed(data);
        Formatter formatter = Formatter.initialize();
        FeedFormatExceptionType type = getExceptionByProcessFeed(formatter, feed);
        Assert.assertEquals(type, FeedFormatExceptionType.INTERSECT_METADATA_ITEM_POSITION);
    }

    @Test
    public void testLoadCustomWrapper() throws FeedFormatException {
        String data = FeedService.loadDataFromResourceFile("test_custom_type.json");
        Feed feed = FeedService.readSingleFeed(data);
        Formatter formatter = Formatter.initialize();
        formatter.loadCustomWrapper(new AbstractTagWrapper() {
            @Override
            protected String getWrapTag() {
                return "p";
            }

            @Override
            public String getWrappedType() {
                return "hashtag";
            }
        });
        String formatData = formatter.processFeed(feed);
        Assert.assertEquals(formatData, ExpectedResult.CUSTOM_WRAPPER_LOAD);
    }

    @Test
    public void testMainSampleData() throws FeedFormatException {
        String data = FeedService.loadDataFromResourceFile("main_sample.json");
        Feed feed = FeedService.readSingleFeed(data);
        Formatter formatter = Formatter.initialize();
        String formatData = formatter.processFeed(feed);
        Assert.assertEquals(formatData, ExpectedResult.TEST_MAIN_SAMPLE);
    }

    @Test
    public void testNegativeMetadata() {
        String data = FeedService.loadDataFromResourceFile("test_negative_position.json");
        Feed feed = FeedService.readSingleFeed(data);
        Formatter formatter = Formatter.initialize();
        FeedFormatExceptionType type = getExceptionByProcessFeed(formatter, feed);
        Assert.assertEquals(type, FeedFormatExceptionType.NEGATIVE_BEGIN_POSITION);
    }

    @Test
    public void testGreaterBeginPosition() {
        String data = FeedService.loadDataFromResourceFile("test_greater_begin.json");
        Feed feed = FeedService.readSingleFeed(data);
        Formatter formatter = Formatter.initialize();
        FeedFormatExceptionType type = getExceptionByProcessFeed(formatter, feed);
        Assert.assertEquals(type, FeedFormatExceptionType.GREATER_BEGIN_POSITION);
    }

    @Test
    public void testUnknownType() {
        String data = FeedService.loadDataFromResourceFile("test_unregistered_type.json");
        Feed feed = FeedService.readSingleFeed(data);
        Formatter formatter = Formatter.initialize();
        FeedFormatExceptionType type = getExceptionByProcessFeed(formatter, feed);
        Assert.assertEquals(type, FeedFormatExceptionType.FORMAT_TYPE_IS_NOT_DETERMINE);
    }
}
