package com.simbirsoft;

public interface ExpectedResult {
    String CUSTOM_WRAPPER_LOAD = "<strong>Obama<strong/> visited <strong>Facebook<strong/> headquarters: <a href=\"http://bit.ly/xyz\">http://bit.ly/xyz<a/> @<a href=\"http://twitter.com/elversatile\">elversatile<a/> <p>#interview<p/>";
    String TEST_MAIN_SAMPLE = "<strong>Obama<strong/> visited <strong>Facebook<strong/> headquarters: <a href=\"http://bit.ly/xyz\">http://bit.ly/xyz<a/> @<a href=\"http://twitter.com/elversatile\">elversatile<a/>";
}
