package com.simbirsoft;

import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;

public class Main {
    private static final String FILE_ARG = "file";
    private static final String RESOURCE_ARG = "resource";

    public static void main(String[] args) {
        Options options = new Options();
        Option r = Option.builder()
                .longOpt(FILE_ARG)
                .argName("file")
                .hasArg()
                .desc("add resource" )
                .build();

        Option f = Option.builder()
                .longOpt(RESOURCE_ARG)
                .argName("file")
                .hasArg()
                .desc("add file" )
                .build();

        options.addOption(r);
        options.addOption(f);
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse( options, args);
            boolean isResource = false;
            boolean useSampleData = true;
            String resourceName = StringUtils.EMPTY;
            if(cmd.hasOption(RESOURCE_ARG)) {
                resourceName = cmd.getOptionValue(RESOURCE_ARG);
                isResource = true;
                useSampleData = false;
            } else if(cmd.hasOption(FILE_ARG)) {
                resourceName = cmd.getOptionValue(FILE_ARG);
                useSampleData = false;
            }
            FeedService feedService = new FeedService();
            feedService.formatFeed(resourceName, isResource, useSampleData);
        } catch (ParseException e) {
            System.out.println(e);
        }
    }

}
