package com.simbirsoft;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbirsoft.format.model.Feed;
import com.simbirsoft.format.exception.FeedFormatException;
import com.simbirsoft.format.Formatter;
import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class FeedService {
    public static final String SAMPLE_RESOURCE_NAME = "sample.json";
    public static final ObjectMapper mapper = new ObjectMapper();

    public void formatFeed(String resourceName, boolean isResource, boolean useSampleData) {
        String data = null;
        if (useSampleData) {
            data = loadDataFromResourceFile(SAMPLE_RESOURCE_NAME);
        } else if (isResource) {
            data = loadDataFromResourceFile(resourceName);
        } else {
            data = loadDataFromFile(resourceName);
        }

        Feed feed = readSingleFeed(data);
        Formatter formatter = Formatter.initialize();
        try {
            String formatData = formatter.processFeed(feed);
            print(formatData);
        } catch (FeedFormatException e) {
            printException(e.getMessage());
        }
    }

    private static void print(String s) {
        System.out.println(s);
    }

    private static void printException(String s) {
        System.err.print(s);
    }

    public static String loadDataFromResourceFile(String resourceName) {
        ClassLoader classLoader = FeedService.class.getClassLoader();
        String data = null;
        try (InputStream inputStream = classLoader.getResourceAsStream(resourceName)) {
            data = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
        } catch (IOException e) {
            print(e.getMessage());
        }
        return data;
    }

    private static String loadDataFromFile(String fileName) {
        String data = null;
        try (InputStream inputStream = new FileInputStream(fileName)) {
            data = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
        } catch (IOException e) {
            print(e.getMessage());
        }
        return data;
    }

    public static Feed readSingleFeed(String data) {
        Feed feed = null;
        try {
            feed = mapper.readValue(data, Feed.class);
        } catch (IOException e) {
            print(e.getMessage());
        }
        return feed;
    }
}
