package com.simbirsoft.format.exception;

public class FeedFormatException extends Exception {
    private FeedFormatExceptionType type;

    public FeedFormatException(String message) {
        super(message);
    }

    public FeedFormatException(FeedFormatExceptionType type) {
        super(type.getMessage());
        this.type = type;
    }

    public FeedFormatExceptionType getType() {
        return type;
    }
}
