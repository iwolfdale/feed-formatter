package com.simbirsoft.format.exception;

public enum FeedFormatExceptionType {
    POSITION_IS_NULL("Begin or end position is null"),
    DATA_IS_NULL("Data for format is null"),
    GREATER_BEGIN_POSITION("The begin position is greater than the end"),
    NEGATIVE_BEGIN_POSITION("The begin position is less than zero"),
    GREATER_END_POSITION("The end position is greater than the format data string"),
    FORMAT_TYPE_IS_NULL("It is not possible to determine the wrapped type, type is null"),
    FORMAT_TYPE_IS_NOT_DETERMINE("It is not possible to determine the wrapped type. Use custom wrappers for new types"),
    INTERSECT_METADATA_ITEM_POSITION("Incorrect metadata. Metadata item positions intercect.");

    private String message;
    FeedFormatExceptionType(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
