package com.simbirsoft.format.wrapper;

public interface Wrapper {
    interface BaseWrappedTypes {
        String ENTITY = "entity";
        String TWITTER_USERNAME = "twitter_username";
        String LINK = "link";
    }

    interface Tags {
        String A = "a";
        String STRONG = "strong";
    }

    String wrap(String wrappedPart);
    String getWrappedType();
}
