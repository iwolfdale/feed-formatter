package com.simbirsoft.format.wrapper.impl;

public class LinkWrapper extends AbstractTagWrapper {

    @Override
    public String getWrappedType() {
        return BaseWrappedTypes.LINK;
    }

    @Override
    protected String getWrapTag() {
        return Tags.A;
    }

    @Override
    protected String getTagAttributes(String part) {
        return String.format("href=\"%s\"", part);
    }
}
