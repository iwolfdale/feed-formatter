package com.simbirsoft.format.wrapper.impl;

import com.simbirsoft.format.wrapper.Wrapper;
import org.apache.commons.lang3.StringUtils;

public abstract class AbstractTagWrapper implements Wrapper {
    public static final String PART_FORMAT = "<%1$s%3$s>%2$s<%1$s/>";

    @Override
    public String wrap(String wrappedPart) {
        String tag = getWrapTag();
        String tagAttributes = getTagAttributes(wrappedPart);
        String tagInnerContent = getTagInnerContent(wrappedPart);
        if (!StringUtils.isEmpty(tagAttributes)) {
            tagAttributes = StringUtils.SPACE + tagAttributes;
        }
        return String.format(PART_FORMAT, tag, tagInnerContent, tagAttributes);
    }

    protected abstract String getWrapTag();

    protected String getTagInnerContent(String part) {
        return part;
    }

    protected String getTagAttributes(String part) {
        return StringUtils.EMPTY;
    }


}
