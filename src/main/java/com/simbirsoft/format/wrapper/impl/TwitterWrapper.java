package com.simbirsoft.format.wrapper.impl;

public class TwitterWrapper extends AbstractTagWrapper {

    @Override
    public String getWrappedType() {
        return BaseWrappedTypes.TWITTER_USERNAME;
    }

    @Override
    protected String getWrapTag() {
        return Tags.A;
    }

    @Override
    protected String getTagAttributes(String part) {
        return String.format("href=\"http://twitter.com/%s\"", part);
    }
}
