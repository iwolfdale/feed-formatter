package com.simbirsoft.format.wrapper.impl;

public class EntityWrapper extends AbstractTagWrapper {

    @Override
    public String getWrappedType() {
        return BaseWrappedTypes.ENTITY;
    }

    @Override
    protected String getWrapTag() {
        return Tags.STRONG;
    }
}
