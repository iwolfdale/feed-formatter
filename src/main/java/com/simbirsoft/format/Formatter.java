package com.simbirsoft.format;

import com.simbirsoft.format.exception.FeedFormatException;
import com.simbirsoft.format.model.Feed;
import com.simbirsoft.format.model.FeedMetaData;
import com.simbirsoft.format.wrapper.Wrapper;
import com.simbirsoft.format.wrapper.impl.EntityWrapper;
import com.simbirsoft.format.wrapper.impl.LinkWrapper;
import com.simbirsoft.format.wrapper.impl.TwitterWrapper;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Formatter {
    private Map<String, Wrapper> wrappers = new HashMap<>();;

    private Formatter() {}

    public static Formatter initialize() {
        Formatter formatter = new Formatter();
        formatter.initializeWrapper(new EntityWrapper());
        formatter.initializeWrapper(new TwitterWrapper());
        formatter.initializeWrapper(new LinkWrapper());
        return formatter;
    }

    public String processFeed(Feed feed) throws FeedFormatException {
        FeedDataValidator feedDataValidator = new FeedDataValidator(wrappers.keySet());
        feedDataValidator.validateFeedData(feed);
        return formatFeed(feed);
    }

    private String formatFeed(Feed feed) throws FeedFormatException {
        String data = feed.getData();
        StringBuilder stringBuilder = new StringBuilder(data);
        List<FeedMetaData> sortedMetaData = sortMetaData(feed.getMetaData());
        for(FeedMetaData feedMetaData : sortedMetaData) {
            Wrapper wrapper = resolveWrapper(feedMetaData);
            String dataPart = data.substring(feedMetaData.getBegin(), feedMetaData.getEnd());
            String wrapped = wrapper.wrap(dataPart);
            stringBuilder.replace(feedMetaData.getBegin(), feedMetaData.getEnd(), wrapped);
        }
        return stringBuilder.toString();
    }

    private Wrapper resolveWrapper(FeedMetaData feedMetaData) throws FeedFormatException {
        Wrapper wrapper = wrappers.get(feedMetaData.getType());
        if (wrapper == null) {
            throw new FeedFormatException(String.format("Don't find wrapper for type \"%s\"", feedMetaData.getType()));
        }

        return wrapper;
    }

    private List<FeedMetaData> sortMetaData(List<FeedMetaData> metaData) {
        metaData.sort(Comparator.comparing(FeedMetaData::getBegin).reversed());
        return metaData;
    }

    private void initializeWrapper(Wrapper wrapper) {
        wrappers.put(wrapper.getWrappedType(), wrapper);
    }

    public Formatter loadCustomWrapper(Wrapper wrapper) {
        initializeWrapper(wrapper);
        return this;
    }
}
