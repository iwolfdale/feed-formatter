package com.simbirsoft.format.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Feed {
    @JsonProperty("data")
    private String data;

    @JsonProperty("metadata")
    private List<FeedMetaData> metaData;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public List<FeedMetaData> getMetaData() {
        return metaData;
    }

    public void setMetaData(List<FeedMetaData> metaData) {
        this.metaData = metaData;
    }
}
