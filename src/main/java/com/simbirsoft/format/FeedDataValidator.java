package com.simbirsoft.format;

import com.simbirsoft.format.exception.FeedFormatException;
import com.simbirsoft.format.exception.FeedFormatExceptionType;
import com.simbirsoft.format.model.Feed;
import com.simbirsoft.format.model.FeedMetaData;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class FeedDataValidator {
    private Set<String> loadedWrappers;

    public FeedDataValidator(Set<String> loadedWrappers) {
        this.loadedWrappers = loadedWrappers;
    }

    private void validateFeedMetaDataIntersect(List<FeedMetaData> metaDataList) throws FeedFormatException {
        Map<UUID, FeedMetaData> metaDataMap = metaDataList
                .stream().collect(Collectors.toMap(e -> UUID.randomUUID(), e -> e));
        for(Map.Entry<UUID, FeedMetaData> currentEntry : metaDataMap.entrySet()) {
            for(Map.Entry<UUID, FeedMetaData> entry : metaDataMap.entrySet()) {
                if (!entry.getKey().equals(currentEntry.getKey())) {
                    FeedMetaData metaData = entry.getValue();
                    FeedMetaData currentMetaData = currentEntry.getValue();
                    if (currentMetaData.getBegin() >= metaData.getBegin() && currentMetaData.getBegin() <= metaData.getEnd()) {
                        throw new FeedFormatException(FeedFormatExceptionType.INTERSECT_METADATA_ITEM_POSITION);
                    }
                    if (currentMetaData.getEnd() >= metaData.getBegin() && currentMetaData.getEnd() <= metaData.getEnd()) {
                        throw new FeedFormatException(FeedFormatExceptionType.INTERSECT_METADATA_ITEM_POSITION);
                    }
                }
            }
        }
    }

    private void validateFeedMetaData(FeedMetaData metaData, String data) throws FeedFormatException {
        if (metaData.getBegin() == null || metaData.getEnd() == null) {
            throw new FeedFormatException(FeedFormatExceptionType.POSITION_IS_NULL);
        }

        if (metaData.getBegin() < 0) {
            throw new FeedFormatException(FeedFormatExceptionType.NEGATIVE_BEGIN_POSITION);
        }

        if (metaData.getEnd() > data.length()) {
            throw new FeedFormatException(FeedFormatExceptionType.GREATER_END_POSITION);
        }

        if (metaData.getType() == null) {
            throw new FeedFormatException(FeedFormatExceptionType.FORMAT_TYPE_IS_NULL);
        }

        if (!loadedWrappers.contains(metaData.getType())) {
            throw new FeedFormatException(FeedFormatExceptionType.FORMAT_TYPE_IS_NOT_DETERMINE);
        }

        if (metaData.getBegin() > metaData.getEnd()) {
            throw new FeedFormatException(FeedFormatExceptionType.GREATER_BEGIN_POSITION);
        }
    }

    public void validateFeedData(Feed feed) throws FeedFormatException {
        String data = feed.getData();
        if (data == null) {
            throw new FeedFormatException(FeedFormatExceptionType.DATA_IS_NULL);
        }

        if (feed.getMetaData() != null) {
            for (FeedMetaData metaData : feed.getMetaData()) {
                validateFeedMetaData(metaData, data);
            }
        }
        validateFeedMetaDataIntersect(feed.getMetaData());
    }
}
