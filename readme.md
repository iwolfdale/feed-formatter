## Сборка и запуск проекта
1. Проект использует java 8, для сборки используется maven
2. Перейти в директорию с проектом и собрать проект командой mvn clean install
3. Перейти в директорию target и запустить приложение командой java -jar feed-formatter-jar-with-dependencies.jar -resource sample.json
4. В качестве входных данных может выступать предопределенный ресурс(аргумент -resource test.json) или файл(аргумент -file C:\test_file.json). 
Если не указан источник входных данных по-умолчанию используется ресурс sample.json

**Формат входного файла(на примере sample.json):**
```javascript
{
  "data": "Obama visited Facebook headquarters: http://bit.ly/xyz @elversatile",
  "metadata": [
    {
      "begin": "14",
      "end": "22",
      "type": "entity"
    },
    {
      "begin": "0",
      "end": "5",
      "type": "entity"
    },
    {
      "begin": "56",
      "end": "67",
      "type": "twitter_username"
    },
    {
      "begin": "37",
      "end": "54",
      "type": "link"
    }
  ]
}
```

